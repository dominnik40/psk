<?php

require_once "Repository.php";
require_once __DIR__.'//..//Models//Team.php';
require_once __DIR__.'//..//Models//Cost.php';


class CostRepository extends Repository {

public function addCost($team,$name,$value,$user)
{
    $stmt = $this->database->connect()->prepare('
            INSERT INTO costs (team,name,value,user) VALUES ( :team,:name,:value,:user)');
        $stmt->bindParam(':team', $team, PDO::PARAM_STR);
        $stmt->bindParam(':name', $name, PDO::PARAM_STR);
        $stmt->bindParam(':value', $value, PDO::PARAM_STR);
        $stmt->bindParam(':user', $user, PDO::PARAM_STR);
        $stmt->execute();


        return true;
}

public function getCostsByTeam($id): array {
    $result = [];
    $stmt = $this->database->connect()->prepare('
        SELECT costs.* FROM  costs WHERE costs.team=:id
    ');
    $stmt->bindParam(':id', $id, PDO::PARAM_STR);
    $stmt->execute();
    $costs = $stmt->fetchAll(PDO::FETCH_ASSOC);

    foreach ($costs as $cost) {
        $result[] = new Cost(

            $cost['name'],
            $cost['value'],
            $cost['user'],
            $cost['id']
        );
    }

    return $result;
}

}