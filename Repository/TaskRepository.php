<?php

require_once "Repository.php";
require_once __DIR__.'//..//Models//Team.php';
require_once __DIR__.'//..//Models//Task.php';


class TaskRepository extends Repository {

public function addTask($team,$name,$status)
{
    $stmt = $this->database->connect()->prepare('
            INSERT INTO tasks (team,name,status) VALUES ( :team,:name,:status)');
        $stmt->bindParam(':team', $team, PDO::PARAM_STR);
        $stmt->bindParam(':name', $name, PDO::PARAM_STR);
        $stmt->bindParam(':status', $status, PDO::PARAM_STR);
        $stmt->execute();


        return true;
}

public function getTasksByTeam($id): array {
    $result = [];
    $stmt = $this->database->connect()->prepare('
        SELECT tasks.* FROM  tasks WHERE tasks.team=:id
    ');
    $stmt->bindParam(':id', $id, PDO::PARAM_STR);
    $stmt->execute();
    $tasks = $stmt->fetchAll(PDO::FETCH_ASSOC);

    foreach ($tasks as $task) {
        $result[] = new Task(

            $task['name'],
            $task['team'],
            $task['status'],
            $task['id']
        );
    }

    return $result;
}

}