<?php

require_once "Repository.php";
require_once __DIR__.'//..//Models//Team.php';

class TeamRepository extends Repository {

    public function getTeam(string $id): ?Team 
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM teams WHERE id = :id
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_STR);
        $stmt->execute();

        $team = $stmt->fetch(PDO::FETCH_ASSOC);

        if($team == false) {
            return null;
        }

        return new Team(
            $team['name'],
            $team['id']
        );
    }

    public function getTeamByName(string $name): ?Team 
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM teams WHERE name = :name
        ');
        $stmt->bindParam(':name', $name, PDO::PARAM_STR);
        $stmt->execute();

        $team = $stmt->fetch(PDO::FETCH_ASSOC);

        if($team == false) {
            return null;
        }

        return new Team(
            $team['name'],
            $team['id']
        );
    }

    public function getTeams(): array {
        $result = [];
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM users
        ');
        $stmt->execute();
        $users = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($users as $user) {
            $result[] = new User(
                $user['email'],
                $user['password'],
                $user['id']
            );
        }

        return $result;
    }

    public function getTeamsByUser($id): array {
        $result = [];
        $stmt = $this->database->connect()->prepare('
            SELECT teams.id, teams.name FROM teams, user_has_team WHERE user_has_team.user=:id AND user_has_team.team=teams.id 
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_STR);
        $stmt->execute();
        $teams = $stmt->fetchAll(PDO::FETCH_ASSOC);


        foreach ($teams as $team) {
            $result[] = new Team(
        
                $team['name'],
                $team['id']
            );
        }

        
        return $result;
    }

    public function addTeam($team)
    {
        $name = $team->getName();

        $stmt = $this->database->connect()->prepare('
            INSERT INTO teams (name) VALUES ( :name)');
        $stmt->bindParam(':name', $name, PDO::PARAM_STR);
        $stmt->execute();

        $team = $this->getTeamByName($name);

        return $team;
    }

    

    public function addMember($team,$user)
    {
    

        $stmt = $this->database->connect()->prepare('
            INSERT INTO user_has_team (team,user) VALUES ( :team,:user)');
        $stmt->bindParam(':team', $team, PDO::PARAM_STR);
        $stmt->bindParam(':user', $user, PDO::PARAM_STR);
        $stmt->execute();


        return true;
    }

    public function deleteALLMembers($team)
    {
    

        $stmt = $this->database->connect()->prepare('
            DELETE FROM user_has_team WHERE team=:team');
        $stmt->bindParam(':team', $team, PDO::PARAM_STR);
        $stmt->bindParam(':user', $user, PDO::PARAM_STR);
        $stmt->execute();


        return true;
    }
}