<!DOCTYPE html>
<head>
    <meta charset="UTF-8">

<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<!-- Google Fonts -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.12.0/css/mdb.min.css" rel="stylesheet">
<title> Board </title>
<link href="/psk/Public/Style/style.css" rel="stylesheet">
</head>
<body>
<div class="container-fluid">
<div class="row mt-5 mb-5 pl-5">
    <div class="col-7">
        <a href="?page=board">
        <img src="/psk/Public/Img/logo.PNG">
</a>
    </div>
    
    <div class="col text-right pr-5">
        <a id="btn-login" href="?page=board" class="btn btn-primary"> Wstecz </a>
    </div>
</div>

<div class="row p-4">
    <div class="col-10 ml-auto mr-auto bg-primary p-5 rounded">

    <h2> Edytuj team </h2>

    <form class="col-6 mt-4 mb-4" method="POST" action="?page=editTeam&team=<?= $team->getId() ?>">
        <div class="form-group">
            <label class="text-white"> Nazwa </label>
            <input type="text" value="<?= $team->getName() ?>" disabled class="form-control">
        </dvi>

        <h4 class="mt-4 mb-4"> Wybierz członków teamu </h5>
        <?php
            foreach($users as $user)
            {

            if ($user->getEmail() == $_SESSION['id'])
            {
                continue;
            }

        ?>
<div class="form-check mt-3 mb-3">
        <?php

            if ($user->check == true)
            {
                $check = 'checked';
            } else
            {
                $check = '';
            }

            ?>
  <input class="form-check-input" <?= $check ?> type="checkbox" name="user-<?= $user->getId() ?>" value="<?= $user->getId() ?>" id="defaultCheck2">
  <label class="form-check-label text-white" for="defaultCheck2">
    <?= $user->getEmail() ?>
  </label>
            </div>
        <?php
            }
        ?>

        <div class="form-group">
            <button class="btn btn-success"> Dodaj </button>
        </dvi>

    </form>


</div>
</div>
<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.12.0/js/mdb.min.js"></script>
</body>
</html>