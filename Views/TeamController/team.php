<!DOCTYPE html>
<head>
    <meta charset="UTF-8">

<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<!-- Google Fonts -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.12.0/css/mdb.min.css" rel="stylesheet">
<title> Board </title>
<link href="/psk/Public/Style/style.css" rel="stylesheet">
</head>
<body>
<div class="container-fluid">
<div class="row mt-5 mb-5 pl-5">
    <div class="col-7">
    <a href="?page=board">
        <img src="/psk/Public/Img/logo.PNG">
</a>    </div>
    
    <div class="col text-right pr-5">
        <a id="btn-login" href="?page=board" class="btn btn-primary"> Wstecz </a>
    </div>
</div>

<div class="row p-4">
    <h2> <?= $team->getName() ?></h2>

    <div class="col-12">
    <div class="row">

    <?php
        foreach($users as $user)
        {
    ?>
    <div class="col-3 m-4 p-4 shadow">
        <a href="?page=user&user=<?= $user->getId(); ?>">

            <div class="col-12 text-center mt-4 mb-4">
                <i class="fas fa-user fa-8x"> </i>
            </div>
            <div class="col-12 text-center"> <?= $user->getEmail() ?> </div>
        </a>


    </div>

    <?php
    }
    ?>


<div class="col-3 m-4 p-4 shadow">

    <a href="?page=addCost&team=<?= $team->getId() ?>">
    <div class="col-12 text-center mt-4 mb-4">
        <i class="fas fa-plus-circle fa-8x"></i>
    </div>
    <div class="col-12 text-center"> Dodaj koszt </div>
    </a>

    
</div>

<div class="col-3 m-4 p-4 shadow">

    <a href="?page=addTask&team=<?= $team->getId() ?>">
    <div class="col-12 text-center mt-4 mb-4">
        <i class="fas fa-plus-circle fa-8x"></i>
    </div>
    <div class="col-12 text-center"> Dodaj zadanie </div>
    </a>

    
</div>

<div class="col-3 m-4 p-4 shadow">

    <a href="?page=editTeam&team=<?= $team->getId() ?>">
    <div class="col-12 text-center mt-4 mb-4">
        <i class="fas fa-plus-circle fa-8x"></i>
    </div>
    <div class="col-12 text-center"> Edytuj team </div>
    </a>

    
</div>

    </div>
    <h4 class="mt-4"> Wszystkie koszty </h4>
    <?php
        if (count($costs) > 0)
        {

    ?>
    <div class="col-8 ml-5 mr-auto mt-4">
    <table class="table col-6">
        <thead>
        <tr>
        <th> Nazwa </th>
        <th> Wartość </th>
</tr>
</thead>
        <?php
            foreach($costs as $cost)
            {
        ?>
        <tr>
                <td width="400px">
                <?= $cost->getName() ?>
            </td>
            
            <td>
                <?= $cost->getValue() ?> zł
            </td>
            </tr>

        <?php
        }
        ?>

    </table>

</div>
        <?php

    } else
    {
        echo 'Nie ma żadnych kosztów';
    }

    ?>

<h4 class="mt-4"> Wszystkie zadania </h4>
    <?php
        if (count($tasks) > 0)
        {

    ?>
    <div class="col-8 ml-5 mr-auto mt-4">
    <table class="table col-6">
        <thead>
        <tr>
        <th> Nazwa </th>
        <th> Status </th>
</tr>
</thead>
        <?php
            foreach($tasks as $task)
            {
        ?>
        <tr>
                <td width="400px">
                <?= $task->getName() ?>
            </td>
            
            <td>
                <?= $task->getStatusName() ?> 
            </td>
            </tr>

        <?php
        }
        ?>

    </table>

</div>
        <?php

    } else
    {
        echo 'Nie ma żadnych zadań';
    }

    ?>

    </div>
</div>
</div>
<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.12.0/js/mdb.min.js"></script>
</body>
</html>