<!DOCTYPE html>
<head>
    <meta charset="UTF-8">

<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<!-- Google Fonts -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.12.0/css/mdb.min.css" rel="stylesheet">
<title> Board </title>
<link href="/psk/Public/Style/style.css" rel="stylesheet">
</head>
<body>
<div class="container-fluid">
<div class="row mt-5 mb-5 pl-5">
    <div class="col-7">
    <a href="?page=board">
        <img src="/psk/Public/Img/logo.PNG">
</a>   
    </div>
    
    <div class="col text-right pr-5">
        <a id="btn-login" href="?page=logout" class="btn btn-primary"> Wyloguj się </a>
    </div>
</div>

<div class="row p-4">
    <h2> <?= $user->getEmail() ?> </h2>

    <div class="col-12">
    <div class="row">

    <?php
        foreach($teams as $team)
        {
    ?>
    <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 col-12 m-4 p-4 shadow">
        <a href="?page=team&team=<?= $team->getId(); ?>">

            <div class="col-12 text-center mt-4 mb-4">
                <i class="fas fa-users fa-8x"> </i>
            </div>
            <div class="col-12 text-center"> <?= $team->getName() ?> </div>
        </a>


    </div>

    <?php
    }
    ?>


    </div>
    </div>
</div>
</div>
<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.12.0/js/mdb.min.js"></script>
</body>
</html>