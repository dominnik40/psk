<!DOCTYPE html>
<head>
    <meta charset="UTF-8">

<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<!-- Google Fonts -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.12.0/css/mdb.min.css" rel="stylesheet">
<title> Board </title>
<link href="/psk/Public/Style/style.css" rel="stylesheet">
</head>
<body>
<div class="container-fluid">
<div class="row mt-5 mb-5 pl-5">
    <div class="col-7">
        <img src="/psk/Public/Img/logo.PNG">
    </div>
    
    <div class="col text-right pr-5">
        <a id="btn-login" href="?page=login" class="btn btn-primary"> Zaloguj się </a>
    </div>
</div>

<div class="row">
    <div class="col-8 ml-auto mr-auto mt-5 text-center">
        <h1 id="slogan"> WORKTIME - DBAMY O TWÓJ TEAM </h1>

        <a id="btn-register" href="?page=register" class="btn btn-primary text-uppercase mt-5"> Zarejestruj się </a>

    </div>
</div>

</div>

<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.12.0/js/mdb.min.js"></script>
</body>
</html>