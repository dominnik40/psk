

<!DOCTYPE html>
<head>
    <meta charset="UTF-8">

<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<!-- Google Fonts -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.12.0/css/mdb.min.css" rel="stylesheet">
<title> Board </title>
<link href="/psk/Public/Style/style.css" rel="stylesheet">
</head>
<body>
<div class="container-fluid">
<div class="row mt-5 mb-5 pl-5">
    <div class="col-7">
        <img src="/psk/Public/Img/logo.PNG">
    </div>
    
    <div class="col text-right pr-5">
        <a id="btn-login" href="?page=dashboard" class="btn btn-primary text-uppercase rounded-pill"> Wstecz </a>
    </div>
</div>

<div class="row">
    <div id="bg-login" class="col-11 ml-auto mr-auto p-5">
        <h1 id="login-title" class="text-center mt-4 mb-4"> LOGOWANIE </h1>
        <div class="col-4 mt-5 ml-auto mr-auto">
            <form action="?page=login" method="POST">
            
                <div class="form-group">
                    <input class="form-control text-center rounded-pill" name="email" type="email" placeholder="Adres e-mail">
                </div>

                
                <div class="form-group">
                    <input class="form-control text-center rounded-pill" name="password" type="password" placeholder="Hasło">
                </div>
                
                <div class="form-group text-center mt-4 mb-4">
                <button id="btn-login" class="btn rounded-pill " type="submit"> ZATWIERDŹ </button>
                </div>

                <div class="messages text-danger text-center mb-5">
                    <?php
                        if(isset($messages)){
                            foreach($messages as $message) {
                                echo $message;
                            }
                        }
                    ?>
                </div>

            </form>
        </div>
        
    </div>
</div>



<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.12.0/js/mdb.min.js"></script>
</body>
</html>