<?php

require_once 'Controllers//BoardController.php';
require_once 'Controllers//SecurityController.php';
require_once 'Controllers//TeamController.php';
require_once 'Controllers//CostController.php';
require_once 'Controllers//TaskController.php';
require_once 'Controllers//UserController.php';



class Routing {
    private $routes = [];

    public function __construct()
    {
        $this->routes = [
            'board' => [
                'controller' => 'BoardController',
                'action' => 'getAllTeams'
            ],
            'addTeam' => [
                'controller' => 'TeamController',
                'action' => 'addTeam'
            ],
            
            'team' => [
                'controller' => 'TeamController',
                'action' => 'team'
            ],

            
            'user' => [
                'controller' => 'UserController',
                'action' => 'user'
            ],

            'editTeam' => [
                'controller' => 'TeamController',
                'action' => 'editTeam'
            ],

            'addCost' => [
                'controller' => 'CostController',
                'action' => 'addCost'
            ],

            'addTask' => [
                'controller' => 'TaskController',
                'action' => 'addTask'
            ],

            'login' => [
                'controller' => 'SecurityController',
                'action' => 'login'
            ],
            
            'register' => [
                'controller' => 'SecurityController',
                'action' => 'register'
            ],
            'logout' => [
                'controller' => 'SecurityController',
                'action' => 'logout'
            ],
            'dashboard' => [
                'controller' => 'SecurityController',
                'action' => 'dashboard'
            ]
        ];
    }

    public function run()
    {
        $page = isset($_GET['page']) ? $_GET['page'] : 'dashboard';

        if (isset($this->routes[$page])) {
            $controller = $this->routes[$page]['controller'];
            $action = $this->routes[$page]['action'];

            $object = new $controller;
            $object->$action();
        }
    }
}