<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Models//User.php';
require_once __DIR__.'//..//Repository//UserRepository.php';

class SecurityController extends AppController {

    public function login()
    {   
        $userRepository = new UserRepository();


        session_start();

        if (isset($_SESSION['id']))
        {
            $url = "http://$_SERVER[HTTP_HOST]/psk";
            header("Location: {$url}?page=board");
        }


        if ($this->isPost()) {
            $email = $_POST['email'];
            $password = $_POST['password'];

            $user = $userRepository->getUser($email);

            if (!$user) {
                $this->render('login', ['messages' => ['Użytkownik o takim e-mailu nie istnieje']]);
                return;
            }

            if (!password_verify($password, $user->getPassword())) {
                $this->render('login', ['messages' => ['Złe hasło!']]);
                return;
            }


            session_start();
            $_SESSION["id"] = $user->getEmail();

            $url = "http://$_SERVER[HTTP_HOST]/psk";
            header("Location: {$url}?page=board");
            return;
        }

        $this->render('login');
    }

    public function logout()
    {
        session_start();
        session_unset();
        
        $this->render('login', ['messages' => ['Wylogowałeś się!']]);
    }


    public function register ()
    {

        session_start();

        if (isset($_SESSION['id']))
        {
            $url = "http://$_SERVER[HTTP_HOST]/psk";
            header("Location: {$url}?page=board");
        }

        $userRepository = new UserRepository();
        if ($this->isPost())
        {
            $email = $_POST['email'];
            $password = $_POST['password'];
            $password2 = $_POST['password2'];

            if ($password !== $password2)
            {
                $this->render('register', ['messages' => ['Hasła nie są poprawne']]);
                return;
            }

            // SPRAWDZAMY CZY UŻYTKOWNIK JEST DODANY
            $user = $userRepository->getUser($email); 

            if ($user) {
                $this->render('register', ['messages' => ['Użytkownik jest już w bazie']]);
                return;
            }

            $user = new User($_POST['email'],password_hash($_POST['password'],PASSWORD_DEFAULT));

            $userRepository->addUser($user);

            session_start();
            $_SESSION["id"] = $user->getEmail();

            $url = "http://$_SERVER[HTTP_HOST]/psk";
            header("Location: {$url}?page=board");




        }

        
        $this->render('register');
    }

    public function dashboard ()
    {
        $this->render('dashboard');
    }

}