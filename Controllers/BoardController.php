<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Models//Team.php';
require_once __DIR__.'//..//Repository//UserRepository.php';


class BoardController extends AppController {

    public function getAllTeams()
    {   
        $userRepository = new UserRepository();
        session_start();

        if (!isset($_SESSION['id']))
        {
            $url = "http://$_SERVER[HTTP_HOST]/psk";
            header("Location: {$url}?page=login");
        }

        $user = $userRepository->getUser($_SESSION["id"]);
        $teams = $user->getTeams();




        $this->render('board', ['user' => $user,
            'teams' => $teams,
        ]);
    }
}