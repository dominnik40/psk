<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Models//Team.php';
require_once __DIR__.'//..//Repository//TeamRepository.php';
require_once __DIR__.'//..//Repository//UserRepository.php';

class UserController extends AppController {


    public function user()
    {
        $id = $_GET['user'];

        $userRepository = new UserRepository();

        $user = $userRepository->getUserByID($id);
        $teams = $user->getTeams();




        $this->render('user', [
            'user' => $user,
            'teams' => $teams,
        ]);


    }


}