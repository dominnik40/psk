<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Models//Team.php';
require_once __DIR__.'//..//Repository//TeamRepository.php';
require_once __DIR__.'//..//Repository//UserRepository.php';

class CostController extends AppController {


    public function addCost()
    {
        $id = $_GET['team'];
        $teamRepository = new TeamRepository();
        session_start();
        $userRepository = new UserRepository();

        $team = $teamRepository->getTeam($id);

        if ($team == null)
        {
            // NIE MA TAKIEGO TEAMU
            $url = "http://$_SERVER[HTTP_HOST]/psk";
            header("Location: {$url}?page=board");
        }

        if ($this->isPost())
        {
            $value = $_POST['value'];
            $name = $_POST['name'];
            
            $user = $userRepository->getUser($_SESSION["id"]);

            $team->addCost($name,$value,$user->getId());


            $url = "http://$_SERVER[HTTP_HOST]/psk";
            header("Location: {$url}?page=team&team=" . $team->getId());
        }


        $this->render('addCost', [
            'team' => $team,
        ]);

    }


}