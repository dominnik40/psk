<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Models//Team.php';
require_once __DIR__.'//..//Repository//TeamRepository.php';
require_once __DIR__.'//..//Repository//UserRepository.php';

class TeamController extends AppController {

    public function addTeam()
    {
        $userRepository = new UserRepository();
        $teamRepository = new TeamRepository();

        session_start();

        if (!isset($_SESSION['id']))
        {
            $url = "http://$_SERVER[HTTP_HOST]/psk";
            header("Location: {$url}?page=login");
        }
        if ($this->isPost())
        {

            
    
           
    
            $user = $userRepository->getUser($_SESSION["id"]);

            $team = new Team($_POST['name']);
            $team = $teamRepository->addTeam($team);
            $team->addMember($user->getId());

            $users = $userRepository->getUsers();

            foreach ($users as $user)
            {
                if (isset($_POST['user-' . $user->getId()]))
                {
                    $team->addMember($user->getId());
                }
            }


            $url = "http://$_SERVER[HTTP_HOST]/psk";
            header("Location: {$url}?page=team&team=" . $team->getId());
        }

        $users = $userRepository->getUsers();

        $this->render('addTeam',[
            'users' => $users,
        ]);
    }

    public function team()
    {
        $id = $_GET['team'];
        $teamRepository = new TeamRepository();
        $team = $teamRepository->getTeam($id);

        if ($team == null)
        {
            // NIE MA TAKIEGO TEAMU
            $url = "http://$_SERVER[HTTP_HOST]/psk";
            header("Location: {$url}?page=board");
        }

        $users = $team->getAllMembers();
        $costs = $team->getAllCosts();
        $tasks = $team->getAllTasks();

        




        $this->render('team', [
            'team' => $team,
            'users' => $users,
            'costs' => $costs,
            'tasks' => $tasks,
        ]);
    }

    public function editTeam()
    {
        $id = $_GET['team'];
        $teamRepository = new TeamRepository();
        session_start();
        $userRepository = new UserRepository();

        $team = $teamRepository->getTeam($id);

        if ($team == null)
        {
            // NIE MA TAKIEGO TEAMU
            $url = "http://$_SERVER[HTTP_HOST]/psk";
            header("Location: {$url}?page=board");
        }
        $users = $userRepository->getUsers();
        $members = $team->getAllMembers();

        if ($this->isPost())
        {
            $team->deleteAllMembers();

            $user = $userRepository->getUser($_SESSION["id"]);

            $team->addMember($user->getId());

            foreach ($users as $user)
            {
                if (isset($_POST['user-' . $user->getId()]))
                {
                    $team->addMember($user->getId());
                }
            }

            $url = "http://$_SERVER[HTTP_HOST]/psk";
            header("Location: {$url}?page=team&team=". $team->getId());

        }


        foreach ($members as $member)
        {
            $i[] = $member->getId();
        }
        

       foreach ($users as $user)
       {
            if (array_search($user->getId(),$i) == false)
            {
                $user->check = false;
            } else
            {
                $user->check = true;
            }

           
       }


        





        $this->render('editTeam', [
            'team' => $team,
            'users' => $users,
        ]);
    }

}