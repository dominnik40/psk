<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Models//Team.php';
require_once __DIR__.'//..//Repository//TeamRepository.php';
require_once __DIR__.'//..//Repository//UserRepository.php';

class TaskController extends AppController {


    public function addTask()
    {
        $id = $_GET['team'];
        $teamRepository = new TeamRepository();
        session_start();
        $userRepository = new UserRepository();

        $team = $teamRepository->getTeam($id);

        if ($team == null)
        {
            // NIE MA TAKIEGO TEAMU
            $url = "http://$_SERVER[HTTP_HOST]/psk";
            header("Location: {$url}?page=board");
        }

        if ($this->isPost())
        {
            $status = $_POST['status'];
            $name = $_POST['name'];
            

            $team->addTask($name,$status);


            $url = "http://$_SERVER[HTTP_HOST]/psk";
            header("Location: {$url}?page=team&team=" . $team->getId());
        }


        $this->render('addTask', [
            'team' => $team,
        ]);

    }


}