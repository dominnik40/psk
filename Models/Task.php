<?php

require_once __DIR__.'//..//Repository//TeamRepository.php';
require_once __DIR__.'//..//Repository//CostRepository.php';

class Task {
    private $id;
    private $name;


    public function __construct(
        string $name,
        $team,
        $status,
        string $id = null
    ) { 
        $this->name = $name;
        $this->team = $team;
        $this->status = $status;
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string 
    {
        return $this->name;
    }

    

    public function getStatusName(): string 
    {
        $ar = [
            'Zaplanowane',
            'W trakcie',
            'Zakończone',
            'Anulowane'
        ];
        return $ar[$this->status];
    }


}