<?php

require_once __DIR__.'//..//Repository//TeamRepository.php';

class User {
    private $id;
    private $email;
    private $password;

    public function __construct(
        string $email,
        string $password, 
        string $id = null
    ) {
        $this->email = $email;
        $this->password = $password;
        $this->id = $id;
    }

    public function getEmail(): string 
    {
        return $this->email;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTeams()
    {
        $teamRepository = new TeamRepository();
        
        $teams = $teamRepository->getTeamsByUser($this->id);

        return $teams;
    }


}