<?php

require_once __DIR__.'//..//Repository//TeamRepository.php';

class Cost {
    private $id;
    private $name;
    private $team;
    private $value;
    private $user;

    public function __construct(
        string $name,
        $value, 
        $team,
        $user,
        $id = null
    ) {
        $this->name = $name;
        $this->value = $value;
        $this->user = $user;
        $this->team = $team;

        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name  ;
    }

    public function getValue()
    {
        return $this->value  ;
    }

    public function getTeam()
    {
        $teamRepository = new TeamRepository();
        
        $team = $teamRepository->getTeam($this->team);

        return $team;
    }


}