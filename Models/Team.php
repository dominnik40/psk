<?php

require_once __DIR__.'//..//Repository//TeamRepository.php';
require_once __DIR__.'//..//Repository//CostRepository.php';
require_once __DIR__.'//..//Repository//TaskRepository.php';


class Team {
    private $id;
    private $name;


    public function __construct(
        string $name,
        string $id = null
    ) { 
        $this->name = $name;
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string 
    {
        return $this->name;
    }

    public function getAllMembers ()
    {
        $userRepository = new UserRepository();
        
        $users = $userRepository->getUsersByTeam($this->id);
        return $users;
    }

    public function getAllCosts ()
    {
        $costRepository = new CostRepository();
        
        $costs = $costRepository->getCostsByTeam($this->id);
        return $costs;
    }

    public function getAllTasks ()
    {
        $taskRepository = new TaskRepository();
        
        $tasks = $taskRepository->getTasksByTeam($this->id);
        return $tasks;
    }


    
    public function addMember ($id)
    {
        $teamRepository = new TeamRepository();
        
        $teamRepository->addMember($this->id, $id);
        return true;
    }

    public function addCost ($name,$value,$user)
    {
        $costRepository = new CostRepository();
        $costRepository->addCost($this->id, $name,$value,$user);
        return true;
    }

    public function addTask ($name,$status)
    {
        $taskRepository = new TaskRepository();
        $taskRepository->addTask($this->id, $name,$status);
        return true;
    }

    
    public function deleteALLMembers ()
    {
        $teamRepository = new TeamRepository();
        
        $teamRepository->deleteAllMembers($this->id);
        return true;
    }



}